const app = getApp()

Page({
  data: {
    faceUrl: "../resource/images/noneface.png"
  },
  onLoad: function(params) {
    var me=this;
    var user =app.userInfo;
    var serverUrl = app.serverUrl;
    wx.showLoading({
      title: '请等待！',
    });
    wx.request({
      url: serverUrl + '/user/query?userId=' + user.id,
      method: "POST",
      header: {
        'content-type': 'application/json' //默认值
      },
      success: function (res) {
        console.log(res.data);
        wx.hideLoading();
        if (res.data.status == 200) {
          var userInfo=res.data.data;
          var faceUrl = "../resource/images/noneface.png";
          if(userInfo.faceImage!=null&&userInfo.faceImage!=''&&userInfo.faceImage!=undefined){
            faceUrl=serverUrl+userInfo.faceImage;
          }
          me.setData({
            faceUrl:faceUrl,
            fansCounts:userInfo.fansCounts,
            followCounts:userInfo.followCounts,
            receiveLikeCounts:userInfo.receiveLikeCounts,
            nickname: userInfo.nickname
          })
        }
      }
    })
  },
  logout: function() {
    var user = app.userInfo;

    var serverUrl = app.serverUrl;
    wx.showLoading({
      title: '请等待！',
    })
    //调用后端
    wx.request({
      url: serverUrl + '/logout?userId=' + user.id,
      method: "POST",
      header: {
        'content-type': 'application/json' //默认值
      },
      success: function(res) {
        console.log(res.data);
        wx.hideLoading();
        if (res.data.status == 200) {
          wx.showToast({
            title: '注销成功',
            icon:'success',
            duration:2000
          });
          app.userInfo=null;
          //页面跳转
          wx.navigateTo({
            url: '../userLogin/login',
          })
        }
      }
    })
  },
  changeFace:function(){
    var me=this;
    wx.chooseImage({
      count:1,
      sizeType:['compressed'],
      sourceType:['album'],
      success: function(res) {
        var tempFilePaths=res.tempFilePaths;
        console.log(tempFilePaths);
        wx.showLoading({
          title: '上传中。。。',
        })
        var serverUrl=app.serverUrl;
        wx.uploadFile({
          url: serverUrl+'/user/uploadFace?userId='+app.userInfo.id,
          filePath: tempFilePaths[0],
          name: 'file',
          success(res) {
            const data = JSON.parse(res.data);
            console.log(data);
            wx.hideLoading();
            if(data.status==200){
              wx.showToast({
                title: '上传成功！',
                icon:"success"
              });

              var imgUrl=data.data;
              me.setData({
                faceUrl: serverUrl+imgUrl
              })
            }else if(data.status==500){
              wx.showToast({
                title: res.data.msg
              })
            }
          }
        })
      }
    })
  },
  uploadVideo:function(){
    wx.chooseVideo({
      sourceType: ['album'],
      success: function (res) {
        console.log(res);
        var duration=res.duration;
        var tempHeight=res.height;
        var tempWidth=res.width;
        var tempVideoUrl=res.tempFilePath;
        var tempCoverUrl=res.thumbTempFilePath;

        if(duration>16){
          wx.showToast({
            title: '视频长度不能超过15秒',
            icon:"none",
            duration:2500
          })
        }else if(duration<5){
            wx.showToast({
              title: '视频长度太短，请上传超过5秒得视频！',
            icon:"none",
            duration:2500
            })
        }else{
          //打开选择bgm页面
          wx.navigateTo({
            url: '../chooseBgm/chooseBgm?duration='+duration
              + "&tempHeight=" + tempHeight
              +"&tempWidth="+tempWidth
              + "&tempVideoUrl=" + tempVideoUrl
              + "&tempCoverUrl=" + tempCoverUrl
          })  
        }
      }
    })
  }

})
const app = getApp()

Page({
  data: {
    bgmList: [],
    serverUrl: "",
    videoParams: {}
  },

  onLoad: function(params) {


    var me = this;
    console.log(params);
    me.setData({
      videoParams: params
    });

    wx.showLoading({
      title: '请等待...',
    });
    var serverUrl = app.serverUrl;
    var user = app.userInfo;

    // 调用后端
    wx.request({
      url: serverUrl + '/bgm/list',
      method: "POST",
      header: {
        'content-type': 'application/json', // 默认值
      },
      success: function(res) {
        //var data=JSON.parse(res.data);
        console.log(res);
        wx.hideLoading();
        if (res.data.status == 200) {
          var bgmList = res.data.data;
          me.setData({
            bgmList: bgmList,
            serverUrl: serverUrl
          });
        } else if (data.status == 502) {
          wx.showToast({
            title:res.data.msg,
            duration: 2000,
            icon: "none",
            success: function() {
              wx.redirectTo({
                url: '../userLogin/login',
              })
            }
          });
        }
      }
    })
  },
  upload: function(e) {
    var me = this;
    var bgmId = e.detail.value.bgmId;
    var desc = e.detail.value.desc;
    console.log("bgmId:" + bgmId);
    console.log("desc:" + desc);
    var duration = me.data.videoParams.duration;
    var tempHeight = me.data.videoParams.tempHeight;
    var tempWidth = me.data.videoParams.tempWidth;
    var tempVideoUrl = me.data.videoParams.tempVideoUrl;
    var tempCoverUrl = me.data.videoParams.tempCoverUrl;

    //上传短视频
    wx.showLoading({
      title: '上传中...',
    })
    var serverUrl = app.serverUrl;
    wx.uploadFile({
      url: serverUrl + '/video/upload',
      formData: {
        userId: app.userInfo.id,
        bgmId: bgmId,
        desc: desc,
        videoSeconds: duration,
        videoHeight: tempHeight,
        videoWidth: tempWidth
      },
      filePath: tempVideoUrl,
      name: 'file',
      header: {
        'content-type': 'application/json', // 默认值
      },
      success: function(res) {
        var data = JSON.parse(res.data);
        wx.hideLoading();
        console.log(data);
        if (data.status == 200) {
          wx.showToast({
                  title: '上传成功！~~',
                  icon: "success"
                })
                wx.navigateBack({
                  delta: 1,
                })
          // var videoId = data.data;
          // //上传封面
          // wx.showLoading({
          //   title: '上传中...',
          // })
          // wx.uploadFile({
          //   url: serverUrl + '/video/uploadCover',
          //   formData: {
          //     userId: app.userInfo.id,
          //     videoId: videoId,
          //   },
          //   filePath: tempCoverUrl,
          //   name: 'file',
          //   header: {
          //     'content-type': 'application/json', // 默认值
          //   },
          //   success: function(res) {
          //     wx.hideLoading();
          //     var data = JSON.parse(res.data);
          //     console.log(data);
          //     if (data.status == 200) {
          //       wx.showToast({
          //         title: '上传成功！~~',
          //         icon: "success"
          //       })
          //       wx.navigateBack({
          //         delta: 1,
          //       })
          //     } else {
          //       wx.showToast({
          //         title: '上传失败',
          //         icon: "success"
          //       })
          //     }

          //   }

          // })


        } else {
          wx.showToast({
            title: '上传失败',
            icon: "success"
          })
        }

      }
    })
  }
})